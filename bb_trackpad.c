/****************************************************************************
 * bb_trackpad/bb_trackpad.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdbool.h>
#include <string.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/signal.h>
#include <nuttx/fs/fs.h>
#include <nuttx/semaphore.h>

#include <nuttx/irq.h>

#include "bb_trackpad.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* The BlackBerry trackpad appears to follow the ADNS3530 register
 * definitions. One difference appears to be that burst mode is not supported.
 */

/* SPI Registers */

#define REG_PRODUCT_ID             0x00
#define REG_REVISION_ID            0x01
#define REG_MOTION                 0x02
#define REG_DELTA_X                0x03
#define REG_DELTA_Y                0x04

#define REG_CONFIGURATION          0x11

#define REG_OBSERVATION            0x2E

#define REG_INV_REVISION_ID        0x3E
#define REG_INV_PRODUCT_ID         0x3F

#define REG_MOTION_BURST           0x42

/* Register bitfields */

#define REG_MOTION_MOT             (1 << 7)
#define REG_MOTION_OVF             (1 << 4)

#define REG_CONFIGURATION_RES      (1 << 7)

#define REG_OBSERVATION_MODE_RUN   (0 << 6)
#define REG_OBSERVATION_MODE_REST1 (1 << 6)
#define REG_OBSERVATION_MODE_REST2 (2 << 6)
#define REG_OBSERVATION_MODE_REST3 (3 << 6)

/* Internal constants */

#define BB_TRACKPAD_PRODUCT_ID     0x0d    /* BB8250 */
#define BB_TRACKPAD_REVISION       0x02

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct bb_trackpad_dev_s
{
  uint32_t devid;
  struct spi_dev_s *spi;
  struct bb_trackpad_lower_s *lower;
  sem_t exclsem;
  int refcount;

  struct pollfd* pfd[CONFIG_SENSORS_BB_TRACKPAD_NPOLLWAITERS];
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     bb_trackpad_open(FAR struct file *filep);
static int     bb_trackpad_close(FAR struct file *filep);
static ssize_t bb_trackpad_read(FAR struct file *filep, FAR char *buffer,
                                size_t buflen);
static ssize_t bb_trackpad_write(FAR struct file *filep,
                                 FAR const char *buffer, size_t buflen);
static int     bb_trackpad_poll(FAR struct file *filep, struct pollfd *fds,
                                bool setup);
static int     bb_trackpad_ioctl(FAR struct file *filep, int cmd,
                                 unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations bb_trackpad_fops =
{
  bb_trackpad_open,  /* open */
  bb_trackpad_close, /* close */
  bb_trackpad_read,  /* read */
  bb_trackpad_write, /* write */
  NULL,              /* seek */
  bb_trackpad_ioctl, /* ioctl */
  bb_trackpad_poll   /* poll */
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL             /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: bb_trackpad_configspi
 ****************************************************************************/

static inline void bb_trackpad_configspi(FAR struct spi_dev_s *spi)
{
  SPI_SETMODE(spi, SPIDEV_MODE3);
  SPI_SETBITS(spi, 8);
  SPI_HWFEATURES(spi, 0);
  SPI_SETFREQUENCY(spi, CONFIG_SENSORS_BB_TRACKPAD_SPIFREQUENCY);
}

/****************************************************************************
 * Name: bb_trackpad_getreg
 ****************************************************************************/

void bb_trackpad_getreg(FAR struct bb_trackpad_dev_s *priv,
                        uint8_t regaddr, uint8_t* data, size_t len)
{
  /* If SPI bus is shared then lock and configure it */

  SPI_LOCK(priv->spi, true);
  bb_trackpad_configspi(priv->spi);

  /* Select device */

  SPI_SELECT(priv->spi, priv->devid, true);

  /* Send register to read and get data */

  SPI_SEND(priv->spi, regaddr);
  SPI_RECVBLOCK(priv->spi, data, len);

  /* Deselect device */

  SPI_SELECT(priv->spi, priv->devid, false);

  /* Unlock bus */

  SPI_LOCK(priv->spi, false);
}

/****************************************************************************
 * Name: bb_trackpad_putreg8
 ****************************************************************************/

void bb_trackpad_putreg8(FAR struct bb_trackpad_dev_s *priv, uint8_t regaddr,
                         uint8_t regval)
{
  /* If SPI bus is shared then lock and configure it */

  SPI_LOCK(priv->spi, true);
  bb_trackpad_configspi(priv->spi);

  /* Select device */

  SPI_SELECT(priv->spi, priv->devid, true);

  /* Send register address (with "write" bit set) and set the value */

  SPI_SEND(priv->spi, regaddr | (1 << 7));
  SPI_SEND(priv->spi, regval);

  /* Deselect device */

  SPI_SELECT(priv->spi, priv->devid, false);

  /* Unlock bus */

  SPI_LOCK(priv->spi, false);
}

/****************************************************************************
 * Name: bb_trackpad_getreg8
 ****************************************************************************/

uint8_t bb_trackpad_getreg8(FAR struct bb_trackpad_dev_s *priv,
                            uint8_t regaddr)
{
  uint8_t regval;

  bb_trackpad_getreg(priv, regaddr, (uint8_t*)&regval, 1);

  return regval;
}

/****************************************************************************
 * Name: bb_trackpad_getreg16
 ****************************************************************************/

uint16_t bb_trackpad_getreg16(FAR struct bb_trackpad_dev_s *priv,
                              uint8_t regaddr)
{
  uint16_t regval;

  bb_trackpad_getreg(priv, regaddr, (uint8_t*)&regval, 2);

  return regval;
}

/****************************************************************************
 * Name: bb_trackpad_read
 ****************************************************************************/

static int bb_trackpad_getmotion(struct bb_trackpad_dev_s* dev,
                                 int16_t* dx, int16_t* dy)
{
  int i = 0;
  uint8_t values[3];

  values[1] = values[2] = 0;

  /* Read motion registers in burst mode */

  bb_trackpad_getreg(dev, REG_MOTION_BURST, values, 3);

  *dx = (int8_t)values[2];
  *dy = (int8_t)values[1];

  /* Since internal data may have overflowed, we read in a loop
   * until all data is read. Just in case we don't end up reading
   * indefinitely if motion continues to occur, we read the maximum
   * possible according to datasheet (32 cycles at full resolution)
   */

  while ((values[0] & REG_MOTION_MOT) && (i < 32))
    {
      bb_trackpad_getreg(dev, REG_MOTION_BURST, values, 3);

      *dx += values[2];
      *dy += values[1];

      i++;
    }

  return (i > 0);
}

/****************************************************************************
 * Name: bb_trackpad_identify
 ****************************************************************************/

static int bb_trackpad_identify(struct bb_trackpad_dev_s* dev)
{
  uint8_t regval, regval2;

  /* Check product ID */

  regval = bb_trackpad_getreg8(dev, REG_PRODUCT_ID);

  if (regval != BB_TRACKPAD_PRODUCT_ID)
    {
      snerr("Unexpected product ID: %i\n", regval);
      return -ENODEV;
    }

  /* Verify correct communication */

  regval2 = bb_trackpad_getreg8(dev, REG_INV_PRODUCT_ID);

  if (!(regval ^ regval2))
    {
      snerr("Read test failed\n");
      return -ENODEV;
    }

  /* Check revision */

  regval = bb_trackpad_getreg8(dev, REG_REVISION_ID);

  if (regval != BB_TRACKPAD_REVISION)
    {
      snerr("Unexpected revision: %i\n", regval);
      return -ENODEV;
    }

  regval2 = bb_trackpad_getreg8(dev, REG_INV_REVISION_ID);

  if (!(regval ^ regval2))
    {
      snerr("Read test failed\n");
      return -ENODEV;
    }

  return OK;
}

/****************************************************************************
 * Name: bb_trackpad_reset
 ****************************************************************************/

static void bb_trackpad_reset(struct bb_trackpad_dev_s* dev)
{
  /* Assert reset signal */

  dev->lower->reset(true);

  /* Wait a bit */

  up_mdelay(1);

  /* Deassert reset signal */

  dev->lower->reset(false);
}

/****************************************************************************
 * Name: bb_trackpad_power
 ****************************************************************************/

static void bb_trackpad_power(struct bb_trackpad_dev_s* dev, bool enable)
{
  /* Turn on/off */

  dev->lower->power(enable);

  if (enable)
    {
      /* Wait a bit after power on */

      up_mdelay(100);

      /* Reset to ensure valid state */

      bb_trackpad_reset(dev);
    }
}

/****************************************************************************
 * Name: bb_trackpad_configure
 ****************************************************************************/

static int bb_trackpad_configure(struct bb_trackpad_dev_s* dev)
{
  /* set resolution, etc */

  uint8_t regval;

  regval = bb_trackpad_getreg8(dev, REG_CONFIGURATION);
  bb_trackpad_putreg8(dev, REG_CONFIGURATION, regval | REG_CONFIGURATION_RES);

  return OK;
}

/****************************************************************************
 * Name: bb_trackpad_initialize
 ****************************************************************************/

static int bb_trackpad_initialize(struct bb_trackpad_dev_s* dev)
{
  int ret = OK;
  int16_t dummy[2];

  /* Power on */

  bb_trackpad_power(dev, true);

  /* Make sure this is the trackpad we expect */

  ret = bb_trackpad_identify(dev);

  if (ret < 0)
    {
      return ret;
    }

  /* Configure */

  ret = bb_trackpad_configure(dev);

  if (ret < 0)
    {
      return ret;
    }

  /* Discard motion data and clear motion flag */

  bb_trackpad_getmotion(dev, dummy, dummy + 1);

  return ret;
}

/****************************************************************************
 * Name: bb_trackpad_handler
 ****************************************************************************/

static int bb_trackpad_handler(int irq, FAR void *context, FAR void *arg)
{
  struct bb_trackpad_dev_s* dev = (struct bb_trackpad_dev_s*)arg;
  int i = 0;

  for (i = 0; i < CONFIG_SENSORS_BB_TRACKPAD_NPOLLWAITERS; i++)
    {
      if (dev->pfd[i] && dev->pfd[i]->events & POLLIN)
        {
          dev->pfd[i]->revents = POLLIN;
          nxsem_post(dev->pfd[i]->sem);
        }
    }

  return 0;
}

/****************************************************************************
 * Name: bb_trackpad_open
 ****************************************************************************/

static int bb_trackpad_open(FAR struct file *filep)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = OK;

  priv = (FAR struct bb_trackpad_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  if (priv->refcount == 0)
    {
      ret = bb_trackpad_initialize(priv);
    }

  if (ret > 0)
    {
      priv->refcount++;
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: bb_trackpad_close
 ****************************************************************************/

static int bb_trackpad_close(FAR struct file *filep)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct bb_trackpad_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  priv->refcount--;

  if (priv->refcount == 0)
    {
      bb_trackpad_power(priv, false);
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: bb_trackpad_read
 ****************************************************************************/

static ssize_t bb_trackpad_read(FAR struct file *filep, FAR char *buffer,
                                size_t len)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = 2 * sizeof(int16_t);

  DEBUGASSERT(buffer);

  priv  = (FAR struct bb_trackpad_dev_s *)filep->f_inode->i_private;

  if (len != 4)
    {
      return -ENOSPC;
    }

  nxsem_wait_uninterruptible(&priv->exclsem);

  bb_trackpad_getmotion(priv, (int16_t*)buffer, ((int16_t*)buffer) + 1);

  nxsem_post(&priv->exclsem);

  return (ssize_t)ret;
}

/****************************************************************************
 * Name: bb_trackpad_write
 ****************************************************************************/

static ssize_t bb_trackpad_write(FAR struct file *filep,
                                 FAR const char *buffer, size_t len)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct bb_trackpad_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  /* nothing to do for now */

  nxsem_post(&priv->exclsem);

  return (ssize_t)ret;
}

/****************************************************************************
 * Name: bb_trackpad_poll
 ****************************************************************************/

static int bb_trackpad_poll(FAR struct file *filep, struct pollfd *fds,
                            bool setup)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct bb_trackpad_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  if ((fds->events & POLLIN) == 0)
    {
      ret = -EDEADLK;
      goto out;
    }

  if (setup)
    {
      int i;

      for (i = 0; i < CONFIG_SENSORS_BB_TRACKPAD_NPOLLWAITERS; i++)
        {
          if (!priv->pfd[i])
            {
              priv->pfd[i] = fds;
              fds->priv = &priv->pfd[i];
              break;
            }
        }

      if (i == CONFIG_SENSORS_BB_TRACKPAD_NPOLLWAITERS)
        {
          fds->priv = NULL;
          ret = -EBUSY;
          goto out;
        }
    }
  else if (fds->priv)
    {
      struct pollfd **slot = (struct pollfd **)fds->priv;

      *slot = NULL;
      fds->priv = NULL;
    }

out:
  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Name: bb_trackpad_ioctl
 ****************************************************************************/

static int bb_trackpad_ioctl(FAR struct file *filep, int cmd,
                             unsigned long arg)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct bb_trackpad_dev_s *)filep->f_inode->i_private;

  nxsem_wait_uninterruptible(&priv->exclsem);

  switch(cmd)
    {
      default:
        ret = -EINVAL;
        break;
    }

  nxsem_post(&priv->exclsem);

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: bb_trackpad_register
 *
 * Description:
 *   Register the bb_trackpad driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the bb_trackpad device to be registered.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int bb_trackpad_register(FAR const char *devname, struct spi_dev_s* spi,
                         uint32_t devid, struct bb_trackpad_lower_s* lower)
{
  FAR struct bb_trackpad_dev_s *priv;
  int ret = OK;

  DEBUGASSERT(devname);
  DEBUGASSERT(spi);
  DEBUGASSERT(lower);

  /* Allocate a new bb_trackpad driver instance */

  priv = (FAR struct bb_trackpad_dev_s *)
           kmm_zalloc(sizeof(struct bb_trackpad_dev_s));

  priv->devid = devid;
  priv->spi   = spi;
  priv->lower = lower;
  nxsem_init(&priv->exclsem, 0, 1);

  priv->lower->set_handler(bb_trackpad_handler, priv);

  ret = register_driver(devname, &bb_trackpad_fops, 0666, priv);

  if (ret < 0)
    {
      kmm_free(priv);
      ret = ERROR;
    }

  return ret;
}
