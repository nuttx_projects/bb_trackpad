/****************************************************************************
 * bb_trackpad/bb_trackpad.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __BB_TRACKPAD_C
#define __BB_TRACKPAD_C

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/input/ioctl.h>
#include <nuttx/spi/spi.h>
#include <nuttx/irq.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define BBTRACKPAD_IOCTL_RESET      0   /* Arg: none */
#define BBTRACKPAD_IOCTL_POWER      1   /* Arg: uint8_t (1: on, 0: off) */

/****************************************************************************
 * Public Types
 ****************************************************************************/

typedef void (*bb_trackpad_handler_t)(void *arg);

struct bb_trackpad_lower_s
{
  void (*reset)(bool assert);
  void (*power)(bool on);
  void (*set_handler)(xcpt_t handler, void *arg);
};

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Name: bb_trackpad_register
 *
 * Description:
 *   Register the bb_trackpad character driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the bb_trackpad device to be registered.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int bb_trackpad_register(FAR const char *devname, struct spi_dev_s *spi,
                         uint32_t devid, struct bb_trackpad_lower_s* lower);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __BB_TRACKPAD_C
